package com.virohan.facesample;

import android.app.Application;
import android.content.Context;

import com.google.firebase.FirebaseApp;

public class MyApplication extends Application {
    private static MyApplication singleton;

/*
    public MyApplication getInstance(){
        return singleton;
    }
*/
    @Override
    public void onCreate() {
        super.onCreate();
        singleton = this;
        try {
            FirebaseApp.initializeApp(this);
        }
        catch (Exception e) {
        }
        //FirebaseApp.initializeApp(MyApplication.this);
    }
    public static Context getInstance() {
        return singleton;
    }
}